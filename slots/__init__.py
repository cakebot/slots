from random import choice, randint

emojis = "🍎🍊🍐🍋🍉🍇🍒"  #: the emojis that the game chooses from


def row():
    """
    Returns a row of 3 emojis (as an array).

    Example: ["🍊", "🍉", "🍇"]
    """
    return [choice(emojis), choice(emojis), choice(emojis)]


def result():
    """
    Returns the result of the game.

    This will be an array, consisting of the following data:
    * A number (either 0 or 1), 0 means you lose and 1 means you win
    * The row of emojis selected (see the row function's documentation)
    """
    r = row()

    if (r[0] == r[1] == r[2]):
        return [1, r]
    else:
        if randint(0, 5) == 4:
            # give player actual chance
            ic = str(choice(emojis))
            return [1, [ic, ic, ic]]
        else:
            return [0, r]
