import setuptools

setuptools.setup(
    name='slots',
    version='1.3',
    description='Base code for our slots game',
    packages=setuptools.find_packages(),
    author='Cakebot Team',
    author_email='me@rdil.rocks',
    url='https://github.com/cakebotpro/slots',
    include_package_data=True
)
